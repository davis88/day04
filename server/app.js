/**
*App.js server side
*/


var express = require("express");
var app = express();
// var bodyParser = require("body-parser");

console.log(__dirname);
const NODE_PORT = process.env.PORT || 4000;
console.log(NODE_PORT);
app.use(express.static(__dirname + "/../client/"));

app.use(bodyParser.urlencoded({linit:'50mb', extended:true}));
app.use(bodyParser.json({limit:'50mb'}));

var popQuiz = require("./quiz.json");
console.log(popQuiz);

app.get("/api/popQuiz", (req,res)=>{
    console.log("get popQuiz");
    res.status(200).json(popQuiz);
});

app.post("/api/submitQuiz", (req,res)=>{
    console.log("submit popQuiz");
    console.log(req.body.choice);
    delete popQuiz.correctMesssage;
    var choiceInt = parseInt(req.body.choice);
    var correctAnswer = parseInt(popQuiz.correctAnswer);

    if (choiceInt == correctAnswer){
        console.log("submit Correct");
        popQuiz.correctMessage = "Correct!";
    } else{
        console.log("submit Incorrect");
        popQuiz.correctMessage = "Incorrect!";
    }

    res.status(200).json(popQuiz);
});

app.use((req,res)=>{
    var x = 6;
    try{
        res.send("<h1> Oopps wrong please </h1>");
    }catch(error){
        console.log(error);
        res.status(500).send("ERROR");
    } 
});

app.listen(NODE_PORT, ()=>{
    console.log(`Web App started at ${NODE_PORT}`);
    //console.log("Web App started at " + NODE_PORT);
});